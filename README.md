# INTRO #

This is a small project to experiment with ConnectSDK to be able to play a TED video from my mobile device to any device supporting the following protocols: DLNA, DIAL, SSAP, ECG, AirPlay, Chromecast, UDAP, and webOS second screen protocol.

# Steps to install #

1) clone repo

2) remove connectsdk plugin from the plugins folder if present

3) install npm dependencies: run ```npm install```

4) reinstall connectsdk plugin:  ```cordova plugin add cordova-plugin-connectsdk```

5) install platform to run on

6) Launch app on platform

# Current State #

Please refer to the js comments in www/js/index.js to find out ;)