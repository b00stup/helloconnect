/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

function wireFeatures() {
    //MY LG TV => [TV][LG]42LA620S-ZA
    //My Orange Box => "décodeur TV d'Orange"
    console.log(device.getFriendlyName());
       
}

function launchBrowser() {
    //This does not work on either my Orange box, nor my LG TV
    var command = device.getLauncher().launchBrowser("http://programme-tv.orange.fr/");
}

function launchYoutube() {
    //youtube launches on TV, but video id seems not to be recognized
    //Orange box does not have this service, and throws an message explaining so

    //youtube video ID's I tried which I know work on both youtube for desktop and mobile browsers
    //  k9UOADKGjMw
    //  Vkw4Dqd43nA
    //  SbhH5t_3ddM

    var vidId = "Vkw4Dqd43nA";
    alert("id="+vidId);

    var command = device.getLauncher().launchYouTube(vidId);

    command.success(function (launchSession) {
        console.log("command was successful");
    }).error(function (err) {
        console.log("error: " + err.message);
    });
}

function sendImage() {
    //Launching an image
    var url = "http://www.connectsdk.com/files/9613/9656/8539/test_image.jpg";
    var iconUrl = "http://www.connectsdk.com/files/9613/9656/8539/test_image.jpg";
    var mimeType = "image/jpeg";
    
    device.getMediaPlayer().displayImage (url, mimeType);
}

//This works on orange box, but not on my LG TV
function sendVideo () {
    device.getMediaPlayer().playMedia("http://video.ted.com/talk/podcast/2015Y/None/JillHeinerth_2015Y.mp4", "video/mp4")
    .success(function(launchSession, mediaControl){

        setTimeout(function(){
            
            //Works fine
            mediaControl.pause()
            .success(function(){

                mediaControl.subscribePlayState() 
               .success(function(playState){
                    //alert(playState);
                    console.log("playState");
                    console.log(playState);
                })
                .error(function(err){
                    console.log("error: " + err.message);
                });

                //position is always = 0 on Orange box
                mediaControl.getPosition()
                .success(function(position){
                    //alert(position);
                    console.log("position");
                    console.log(position);                            
                })
                .error(function(err){
                    console.log("error: " + err.message);
                });

                //duration is always = 0 on Orange box (cant start video on LG TV)
                mediaControl.getDuration()
                .success(function(duration){
                    console.log(duration);
                    setTimeout(function(){
                        alert(duration);
                    }, 3000)
                })
                .error(function(err){
                    console.log("error: " + err.message);
                })
            })

        }, 3000);


    })
}

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        app.setupDiscovery();
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        console.log('Received Event: ' + id);
    },
    setupDiscovery: function () {
        ConnectSDK.discoveryManager.startDiscovery();
    },
    showDevicePicker: function () {
        ConnectSDK.discoveryManager.pickDevice().success(function (device) {

            window.device = device;

            if (device.isReady()) { // already connected
                wireFeatures();
            } else {
                device.on("ready", wireFeatures);
                device.connect();
            }
        })
    }
};

app.initialize();